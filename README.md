# borgcheck

Borgcheck is a small utility command that lets you check multiple borg backup repositories in multiple servers, and return clean concise and interpreted results, letting you easily check if all is going good with your backups.

![](./screenshot.png)


## Dependencies

Borgcheck uses `ssh`, `gpg` and [nodejs](https://nodejs.org/), so make sure you've all those installed before starting.


## Usage

  - if you don't have it already, install [nodejs](https://nodejs.org/)
  - clone this repository: `git clone https://framagit.org/squeak/borgcheck.git`
  - cd in the repository just created
  - install dependencies running `npm install`
  - create a configuration file listing your borg repositories to check (see next section for details)
  - make the `borgcheck` script executable with something like `chmod u+x ./borgcheck`
  - run the `borgcheck` command, for example: `./borgcheck --full`


## Configure the list of borg backup repositories

The listing of borg repositories to check should by default be put in a `config.js` file at the root of the borgcheck folder.

This file should list your borg repositories with the following syntax:
```javascript
{
  name: "name-of-the-repo", // used for display, and to be able to easily choose to only check some specific repos
  host: "somehost", // ssh host where the borg repository is located
  directory: "my-backups", // name of the directory containing the borg repository in that host
  passphraseFile: "~/backup-keys/somehost_my-backups_key.gpg", // the path to the file containing the key to decrypt the borg repository
  itemIdMatcher: /^_auto_([^-]*)/, // the prefix that each backup name contains
  items: ["conf", "data", "wordpress", "nextcloud"], // the list of backedup items you expect every day to contain
  // Filling "itemIdMatcher" and "items" allow borgcheck to figure out if some things are missing in each day of backup and to display in a nice visual way.
},
```

For an in-depth example and a good baseline to start building your config file with the appropriate syntax, check the `example-config.js` file.
