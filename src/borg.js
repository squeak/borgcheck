const _ = require("underscore");
const $$ = require("squeak/node");
const utils = require("./utils");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET BORG PASSPHRASE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get a passphrase from the given gpg file (assuming it's in pass format (= first line of file))
    ARGUMENTS: (
      !repository <borgcheck·repository>,
      !callback <function(passphrase<string>)>,
    )
    RETURN: <void>
  */
  getPassphrase: function (repository, callback) {
    // PASSPHRASE ALREADY GOTTEN, SKIP
    if (repository.passphrase) callback(repository.passphrase)
    // GET PASSPHRASE FROM FILE
    else utils.execWrapper(
      // get passphrase from file
      "cat "+ repository.passphraseFile +" | gpg -d 2>/dev/null",
      // message in case of error
      "Error figuring out passphrase for repository: "+ repository.name,
      // callback
      function (stdout) {
        // keep only first line
        repository.passphrase = stdout.split(/\n/)[0];
        // run callback
        callback(repository.passphrase);
      }
    );
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  BORG BACKUPS LIST
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: list borg backups in the given repository
    ARGUMENTS: (
      !repo <borgcheck·repository>,
      ?callback <function(<string> « normal output of "borg list" command »)>,
    )
    RETURN: <void>
  */
  list: function (repo, callback) {
    if (!repo.passphrase) return $$.log.error("Missing passphrase, you must run 'borg.getPassphrase' before running 'borg.list'.");
    utils.execWrapper(
      // list borg backups
      'ssh '+ repo.host +' " '+ utils.addPassphraseToBorgCommand(repo) +' borg list '+ repo.directory +'"',
      // message in case of error
      "Error listing backups in repository: "+ repo.name,
      // callback
      callback
    );
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  BORG BACKUPS INFO
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get info about borg backups in the given repository
    ARGUMENTS: (
      !repo <borgcheck·repository>,
      ?callback <function(<string> « normal output of "borg info" command »)>,
    )
    RETURN: <void>
  */
  info: function (repo, callback) {
    if (!repo.passphrase) return $$.log.error("Missing passphrase, you must run 'borg.getPassphrase' before running 'borg.info'.");
    utils.execWrapper(
      // get borg repo info
      'ssh '+ repo.host +' " '+ utils.addPassphraseToBorgCommand(repo) +' borg info '+ repo.directory +'"',
      // message in case of error
      "Error getting info about backups in repository: "+ repo.name,
      // callback
      callback
    );
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  BORG BACKUPS INFO
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get info about used space by borg backups in the given repository
    ARGUMENTS: (
      !repo <borgcheck·repository>,
      ?callback <function(<string> « normal output of "borg info" command »)>,
    )
    RETURN: <void>
    TODO: maybe could reimplement this using "borg info {repo} --json" flag
  */
  usedSpace: function (repo, callback) {
    if (!repo.passphrase) return $$.log.error("Missing passphrase, you must run 'borg.getPassphrase' before running 'borg.info'.");
    utils.execWrapper(
      // get borg repo info
      'ssh '+ repo.host +' " '+ utils.addPassphraseToBorgCommand(repo) +' borg info '+ repo.directory +'" | grep "All archives:" | awk \'{print $7,$8}\'',
      // message in case of error
      "Error checking used space by backups in repository: "+ repo.name,
      // callback
      callback
    );
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  BORG BACKUPS QUOTA
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get storage quota for the given repository
    ARGUMENTS: (
      repository <borgcheck·repository>,
      callback <function(commandOutput<string>)>,
    )
    RETURN: <void>
  */
  quota: function (repository, callback) {
    utils.execWrapper(
      // get borg backup repo quota
      "ssh "+ repository.host +" borg config "+ repository.directory +" storage_quota",
      // message in case of error
      "Error fetching storage quota for repository: "+ repository.name,
      // callback
      function (stdout) {
        if (!stdout) stdout = "";
        callback(
          // convert quota to a readable way format
          stdout
            // figure out which unit is more adapted
            .replace(/(?<=\d)(\d{12})$/, ".$1 TB")
            .replace(/(?<=\d)(\d{9})$/,  ".$1 GB")
            .replace(/(?<=\d)(\d{6})$/,  ".$1 MB")
            .replace(/(?<=\d)(\d{3})$/,  ".$1 KB")
            // remove useless zero decimals and comma
            .replace(/\.?0*(\s.B)$/, "$1")
        );
      }
    );
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
