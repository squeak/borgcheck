const exec = require("child_process").exec;
const _ = require("underscore");
const $$ = require("squeak/node");
const async = require("async");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EXEC WRAPPER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: wrapper around exec, to simplify it's calling
    ARGUMENTS: (
      !command <string> « command to execute »,
      !errorMessage <string> « message to display in case of error (it will be logged, and the full error will be logged as well) »,
      ?callback <function(stdout)> « executed only if command succeeded »,
      ?errorCallback <function(err, stdout, stderr)> « eventual command to execute on command failure »,
    )
    RETURN: <void>
  */
  execWrapper: function (command, errorMessage, callback, errorCallback) {
    exec(command, function (err, stdout, stderr) {
      if (err) {
        $$.log.error(errorMessage);
        $$.log.error(err);
        if (_.isFunction(errorCallback)) errorCallback(err, stdout, stderr);
      }
      else {
        // clean output of eventual starting and trailing spaces
        if (_.isString(stdout)) stdout = stdout.replace(/^[\s\n]*/, "").replace(/[\s\n]*$/, "");
        if (_.isFunction(callback)) callback(stdout);
      };
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ADD REPO PASSPHRASE TO BORG COMMAND
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: add repo passphrase to a borg command
    ARGUMENTS: ( !repo <borgcheck·repository>, )
    RETURN: <string>
  */
  addPassphraseToBorgCommand: function (repo) {
    return 'BORG_PASSPHRASE=\''+ repo.passphrase.replace(/"/g, "\\\"").replace(/`/g, "\\\`") +'\'';
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REMIND TO CHECK RAW OUTPUT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  remind2checkRaw: function (options) {
    if (!options.raw) $$.log.warning("Make sure to regularly check raw outputs, because the analysis may not identify issues like if a backup was only partial, or corrupted.");
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DO FOR EACH REPO
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: execute a command on one or many repositories, and log it's outputs
    ARGUMENTS: (
      !repositories <borgcheck·repository|borgcheck·repositories> « repository or repositories to process »,
      !whatToDo <function(
        repo <borgcheck·repository> « the repository currently being iterated »,
        callback <function(output <string|string[]> « the output to log, formatted if desired »)> «
          this method must be called from whatToDo body, so the iteration can continue and output be shown by eachRepo
        »,
      )> « what to do on each repo »,
      ?finalCallback <function(ø)> « ran when all finished »,
    )
    RETURN: <void>
  */
  eachRepo: function (repositories, whatToDo, finalCallback) {
    if (!_.isArray(repositories)) repositories = [repositories];

    // DISPLAY LOADING SPINNER
    var spinner = $$.log.node.spinner({
      message: "Analysing repositories: "+ _.pluck(repositories, "name").join(", "),
      color: "yellowBright",
      spinner: "random",
    });

    // ITERATING BACKUP REPOS
    async.eachSeries(repositories, function (repo, eachDone) {
      whatToDo(repo, function (output) {

        spinner.clear();

        // LOG OUTPUTS
        $$.log.line();
        $$.log();
        $$.log.style(repo.name, "bgMagenta");
        if (!_.isArray(output)) output = [output];
        _.each(output, function (outputString) { $$.log(outputString); });
        $$.log();

        // ITERATE NEXT REPO
        eachDone();

      });
    }, function () {
      spinner.destroy(true);
      $$.log.line();
      if (_.isFunction(finalCallback)) finalCallback();
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
