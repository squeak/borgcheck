const _ = require("underscore");
const $$ = require("squeak/node");
const borg = require("./borg");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: give an overview of the asked repo (mainly using "borg info" command)
  ARGUMENTS: (
    !repo <borgcheck·repository>,
    ?done <function(output<string|string[]>)> « when finished called back with commands output passed to it »,
  )
  RETURN: <void>
*/
module.exports = function (repo, done) {
  borg.getPassphrase(repo, function (passphrase) {

    borg.usedSpace(repo, function (spaceUsed) {
      borg.quota(repo, function (repoQuota) {

        // MAKE OUTPUT
        var output = [
          $$.log.node.style(spaceUsed +" / "+ repoQuota, "magenta"),
        ];

        // DONE CALLBACK
        if (_.isFunction(done)) done(output);

      });
    });

  });
};
