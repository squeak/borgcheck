const _ = require("underscore");
const $$ = require("squeak/node");
require("squeak/extension/array");
require("squeak/extension/object");
const dayjs = require("dayjs");
// dayjs.extend(require("dayjs/plugin/isSameOrBefore"));
const borg = require("./borg");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DATE RANGE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// /**
//   DESCRIPTION: get an array of dates between start and end date
//   ARGUMENTS: (
//     !startDate <string>,
//     !endDate <string>,
//   )
//   RETURN: <string[]>
// */
// function datesBetween (startDate, endDate) {
//   var datesArray = [];
//   endDate = dayjs(endDate);
//   var currentDate = dayjs(startDate);
//   while (currentDate.isSameOrBefore(endDate)) {
//     datesArray.push(currentDate.format("YYYY-MM-DD"));
//     currentDate = currentDate.add(1, "day");
//   }
//   return datesArray;
// };

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DETAILS ANALYSER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: convert backups list text into a collection
  ARGUMENTS: (
    !backupsList <string>,
    !repo <borgcheck·repository>,
  )
  RETURN: <{
    name: <string>,
    date: <string> « format is: "YYYY-MM-DD hh:mm:ss" »,
    borgUUID: <string>,
  }[]>,
*/
function convertBackupsListTextToCollection (backupsList, repo) {
  if (!_.isString(backupsList)) {
    $$.log.error("[borgcheck/details/processBackupsList] backupsList is not a string:");
    $$.log.error(backupsList);
    return;
  };

  // convert backups list text to a collection
  return _.map(backupsList.split("\n"), function (line) {
    if (!line) return "";
    var lineObject = {
      raw: line,
      name: $$.match(line, /^[^\s]*/),
      date: line.replace(/^[^\s]*\s*/, "").replace(/\s*[^\s]*$/, "").replace(/^...,\s/, ""),
      borgUUID: $$.match(line, /[^\s]*$/).replace(/^\[/, "").replace(/\]$/, ""),
    };
    if (_.isRegExp(repo.itemIdMatcher)) lineObject.itemId = _.last(repo.itemIdMatcher.exec(lineObject.name));
    lineObject.day = $$.match(lineObject.date, /^\d{4}-\d{2}-\d{2}/);
    return lineObject;
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  ANALYSE BACKUPS LIST
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function analyseBackupsList (backupsList, repo, options) {
  var perdayItemsAmount = repo.items.length;
  // convert list of backups to a collection of objects
  var backupsListCollection = convertBackupsListTextToCollection(backupsList, repo);
  // group objects by day
  var byDay = _.groupBy(backupsListCollection, "day");

  // add any missing date in the last week
  for (var i = 7; i >= 0; i--) {
    var dateToAdd = dayjs().subtract(i, "day").format("YYYY-MM-DD");
    if (!byDay[dateToAdd]) byDay[dateToAdd] = [];
  };
  byDay = $$.object.sort(byDay);

  // generate output lines
  var output = _.map(byDay, function (backups, day) {

    // figure out if some items are missing this day
    var dayUniqItemsNames = _.uniq(_.pluck(backups, "itemId"));
    var missingItems = _.difference(repo.items, dayUniqItemsNames);

    // figure out color of output
    if (!backups.length) var dayOutputColor = "red"
    else if (backups.length < perdayItemsAmount) var dayOutputColor = "yellow"
    else if (backups.length === perdayItemsAmount) var dayOutputColor = "green"
    else var dayOutputColor = "greenBright";

    // make output containing the date and number of items present
    var outputLineArray = [
      $$.log.node.style(
        day +" — "+ backups.length +"/"+ perdayItemsAmount,
        dayOutputColor
      ),
    ];
    
    // if some items are missing
    if (missingItems.length) {
      // add present and absent items to first line
      var presentItems = _.difference(repo.items, missingItems);
      outputLineArray[0] = outputLineArray[0] + " — "
        + $$.log.node.style(presentItems.join(" "), "green")
        + " "
        + $$.log.node.style(missingItems.join(" "), "red")
      ;
      // if in last month, display the raw outputs for this day
      if (dayjs(day).isAfter(dayjs().subtract(1, "month"))) {
        _.each(backups, function (line) { outputLineArray.push(line.raw); });
      };
    };

    // return joined output
    return outputLineArray.join("\n");

  });

  // only show last x days
  if (options.tail) return _.last(
    output,
    _.isNumber(options.tail) ? options.tail : 7
  )
  // show all
  else return output;

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION:
    list created backups in the passed repo
    this method uses the "borg list" command, and can make analysis of it's result
  ARGUMENTS: (
    !repo <borgcheck·repository>,
    ?options <{
      ?raw: <boolean|"only"> «
        if true, will additionaly display raw output from "borg list" command
        if "only", will exclusively display raw output
      »,
      ?tail: <boolean|number> «
        if true, will output details only for the last week (or last 20 lines if raw was asked)
        if it's a number, will output details for this number of days (or number of lines if raw)
      »,
    }>,
    ?done <function(output<string|string[]>)> « when finished called back with commands output passed to it »,
  )
  RETURN: <void>
*/
module.exports = function (repo, options, done) {
  if (!options) options = {};
  borg.getPassphrase(repo, function (passphrase) {

    borg.list(repo, function (backupsList) {

      // MAKE RAW OUTPUT IF ASKED
      if (options.raw) {
        // display last x lines
        if (options.tail) var rawOutput = _.last(
          backupsList.split("\n"),
          _.isNumber(options.tail) ? options.tail : 20
        )
        // display full output
        else var rawOutput = backupsList;
      };

      // MAKE INTERPRETED OUTPUT IF ASKED
      if (options.raw !== "only") var interpretedOutput = analyseBackupsList(backupsList, repo, options);

      // DONE CALLBACK
      var output = $$.array.merge(rawOutput, interpretedOutput); // $$.array.merge handles case if outputs are undefined, arrays or just strings :)
      if (_.isFunction(done)) done(output);

    });

  });
};
