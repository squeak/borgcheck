
module.exports = [

 {
   name: "server-a",
   host: "mybackupserver",
   directory: "backup-server_a",
   passphraseFile: "~/backup-keys/mybackupserver-server_a.gpg",
   itemIdMatcher: /^_auto_([^-]*)/, // in case you use yunohost, this prefix is adapted to the format borg_ynh is using
   items: ["conf", "data", "borg", "nextcloud", "wordpress"],
 },

 {
   name: "server-a",
   host: "workbackupserver",
   directory: "backup-server_a",
   passphraseFile: "~/backup-keys/workbackupserver-server_a.gpg",
   itemIdMatcher: /^_auto_([^-]*)/, // in case you use yunohost, this prefix is adapted to the format borg_ynh is using
   items: ["conf", "data", "borg", "nextcloud", "wordpress"],
 },

 {
   name: "server-b",
   host: "mybackupserver",
   directory: "backup-server_b",
   passphraseFile: "~/backup-keys/mybackupserver-server_b.gpg",
   itemIdMatcher: /^_auto_([^-]*)/, // in case you use yunohost, this prefix is adapted to the format borg_ynh is using
   items: ["conf", "data", "roundcube"],
 },

 {
   name: "personal",
   host: "mybackupserver",
   directory: "backup-personal_stuff",
   passphraseFile: "~/backup-keys/mybackupserver-personal_stuff.gpg",
   itemIdMatcher: /^[^_]*/,
   items: ["documents", "images", "some-dir", "other-dir"],
 },

];
